<?php

class Config
{
    const DIR = __DIR__ . "/../config/";
    private static $_config;

    public static function get($key)
    {
        if(!self::$_config) {
            self::load();
        }

        return isset(self::$_config[$key]) ? self::$_config[$key] : null;
    }

    private static function load()
    {
        if (!is_dir(self::DIR)) {
            throw new Exception(self::DIR . " doesn't exist.");
        }

        $files = array_diff(scandir(self::DIR), array('..', '.'));

        foreach ($files as $file) {
            $path = pathinfo(self::DIR . DIRECTORY_SEPARATOR . $file);
            if ($path["extension"] != "ini") {
                continue;
            }

            if (!is_readable(self::DIR . $path['basename'])) {
                throw new Exception(self::DIR . $path['filename'] . " couldn't be read.");
            }

            self::parseFile($path);
        }
    }

    private function parseFile($file)
    {
        $config = parse_ini_file(self::DIR . $file['basename'], true);

        if ($config === false) {
            throw new Exception("Config file parsing failed.");
        }

        self::$_config[$file['filename']] = $config;
    }
}