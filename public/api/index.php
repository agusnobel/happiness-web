<?php
require_once __DIR__ . '/../../vendor/autoload.php';
require_once __DIR__ . '/../../core/Config.php';

$app = new Silex\Application();
$app['debug'] = true;

//<editor-fold desc="wlan">
$wlan = $app['controllers_factory'];

$wlan->get('/', function() {
    return '';
});

$wlan->get('/{action}', function($action) use ($app) {
    $response = [];

    switch($action) {
        case "scan":
            $cellArray = [];
            $scan = shell_exec("sudo iwlist " . Config::get("config")["wlan"]["interface"] . " scan | grep -oE '(- |ESSID:|Address:|Quality=|Encryption key:).*'");
            $cells = explode("- ", $scan);

            foreach($cells as $index => $cell) {
                if($index == 0) {
                    continue;
                }

                preg_match(
                    '/Address:\s*(([0-9A-Z]{2}:?){6})\s*Quality=(\d+)\/(\d+)\s*Signal level=(-?\d+) dBm\s*Encryption key:(on|off)\s*ESSID:"(.*)"/',
                    $cell, $matches
                );

                $cellArray[] = [
                    "ssid" => $matches[7],
                    "quality" => number_format(intval($matches[3]) / intval($matches[4]) * 100, 0),
                    "signal_strength" => $matches[5],
                    "encryption" => $matches[6],
                    "address" => $matches[1],
                ];
            }

            $response = [
                "cells" => $cellArray
            ];
            break;
    }

    return $app->json($response);
});

$wlan->post('/{action}', function($action) use ($app) {
    $response = [];
    $input = file_get_contents("php://input");
    parse_str($input, $input);

    switch($action) {
        case "connect":
            if(!isset($input["cell"])
                || !isset($input["cell"]["ssid"])
                || !isset($input["password"])
            ) {
                return $app->abort(400);
            }

            $ssid = $input["cell"]["ssid"];
            $password = $input["password"];

            $cmd = sprintf("sudo wpa_passphrase \"%s\" >> /etc/wpa_supplicant/wpa_supplicant.conf", addslashes($ssid));

            $descriptorSpec = array(
                0 => array("pipe", "r"),  // stdin is a pipe that the child will read from
                1 => array("pipe", "w")  // stdout is a pipe that the child will write to
            );
            $process = proc_open($cmd, $descriptorSpec, $pipes);

            fwrite($pipes[0], $password . "\n");
            fclose($pipes[0]);

            $output = stream_get_contents($pipes[1]);
            fclose($pipes[1]);
            proc_close($process);

            shell_exec("sudo reboot");
//            shell_exec("sudo ifdown " . Config::get("config")["wlan"]["interface"]);
//            shell_exec("sudo ifup " . Config::get("config")["wlan"]["interface"]);

            $response = compact("ssid", "password", "output"); // DEBUG
            break;
    }

    return $app->json($response);
});
//</editor-fold>

//<editor-fold desc="wlan">
$remote = $app['controllers_factory'];

$remote->get('/', function() {
    return '';
});

$remote->post('/{action}', function($action) use ($app) {
    $response = [];
    $input = file_get_contents("php://input");
    parse_str($input, $input);
    $client = new GuzzleHttp\Client([
        "base_uri" => "http://" . Config::get("config")["remote_device"]["ip_address"]
    ]);

    switch($action) {
        case "speed":
            if(!isset($input["speed"]) || !is_float(floatval($input["speed"]))) {
                return $app->abort(400);
            }
            $res = $client->get("/speed?params=" . $input["speed"]);

            $response = [
                "message" => "",
                "debug" => [ // DEBUG
                    "response" => $res
                ]
            ];
            break;
        case "rhythm":
            if(!isset($input["rhythm"]) || !is_numeric($input["rhythm"])) {
                return $app->abort(400);
            }
            $res = $client->get("/rhythm?params=" . $input["rhythm"]);

            $response = [
                "message" => "",
                "debug" => [ // DEBUG
                    "response" => $res
                ]
            ];
            break;
        case "vibrating":
            if(!isset($input["vibrating"]) || !is_numeric($input["vibrating"])) {
                return $app->abort(400);
            }
            $res = $client->get("/vibrating?params=" . $input["vibrating"]);

            $response = [
                "message" => "",
                "debug" => [ // DEBUG
                    "response" => $res
                ]
            ];
            break;
        default:
            $response = $client->get("");
            break;
    }

    return $app->json($response);
});
//</editor-fold>

//MOUNTS
$app->mount('/wlan', $wlan);
$app->mount('/remote', $remote);
//END MOUNTS

$app->run();