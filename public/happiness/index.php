<?php
define("VIEWS_DIR", __DIR__ . "/../../views/");

$hasInternet = false;
$connection = @fsockopen("www.example.com", 80);

if ($connection){
    $hasInternet = true;
    fclose($connection);
}

$viewFile = '';

if($hasInternet) {
    $viewFile = VIEWS_DIR . "controls.html";
} else {
    $viewFile = VIEWS_DIR . "settings.html";
}

if(empty($viewFile)) {
    http_response_code(404);
    echo "404 Not Found";
} else {
    echo file_get_contents($viewFile);
}

exit;