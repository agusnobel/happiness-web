(function() {
    window.app = {};
    window.app.vars = {};
    window.fn = {};

    app.init = function() {
        app.vars.cells = [];

        $.material.init();
        $("button[name=\"refresh\"]").on("click", app.refreshHandler);
        $("form#network-connect").on("submit", app.networkConnectHandler);
        app.refreshHandler();
    };

    app.refreshHandler = function(e) {
        if(e) {
            e.preventDefault();
        }

        $("#networks").find(".list-group").find(".list-group-item, .list-group-separator").addClass("old");
        app.isLoading(true);

        $.ajax({
            url: '../api/wlan/scan',
            dataType: 'json',
            success: app.successHandler,
            error: app.errorHandler,
            complete: function() {
                app.isLoading(false);
            }
        });
    };

    app.successHandler = function(data) {
        app.vars.cells = $.merge(app.vars.cells, data.cells);
        fn.mergeCellDuplicates();
        app.vars.cells.sort(fn.sortCellsByQualityAndSignal);
        $.each(window.app.vars.cells, app.addCellToNetworksPanel);
        $("#networks").find(".list-group").find(".old").remove();
    };

    app.addCellToNetworksPanel = function(index, value) {
        if(value["ssid"] == "") {
            return;
        }

        var cell = $("<div>").addClass("list-group-item");
        cell.append("<div class=\"row-action-primary\"><i class='material-icons'>wifi</i></div>");
        cell.append("<div class=\"row-content\"><div class='least-content'></div><h4 class=\"list-group-item-heading\"></h4><p class=\"list-group-item-text\"></p></div>");

        if(value["encryption"] == "on") {
            cell.append("<i class=\"material-icons encryption\">lock</i>");
        }

        var rowContent = cell.find(".row-content");
        rowContent.find(".least-content").text("Quality: " + value["quality"] + "%");
        rowContent.find(".list-group-item-heading").text(value["ssid"]);

        var table = $("<table>");
        var signalText = "Unkown";
        var signalStrength = parseInt(value["signal_strength"]);

        if(signalStrength <= -90) {
            signalText = "Unusable";
        } else if(signalStrength <= -80) {
            signalText = "Very bad";
        } else if(signalStrength <= -70) {
            signalText = "Bad";
        } else if(signalStrength <= -67) {
            signalText = "Okay";
        } else if(signalStrength <= -30) {
            signalText = "Good";
        } else if(signalStrength > -30) {
            signalText = "Optimal";
        }

        table = fn.addLabelValueRowToTable(table, "Signal strength", signalText);
        rowContent.find(".list-group-item-text").append(table);

        cell.on("click", app.onNetworkClickHandler);

        var listGroup = $("#networks").find(".list-group");
        listGroup.append(cell.data("ssid", value["ssid"]));

        if(index + 1 < app.vars.cells.length) {
            listGroup.append("<div class=\"list-group-separator\"></div>");
        }
    };

    app.errorHandler = function() {
        console.log('Error on scanning', arguments);
    };

    app.onNetworkClickHandler = function(e) {
        e.preventDefault();

        var target = $(e.target);

        if(!target.is(".list-group-item")) {
            target = target.parents(".list-group-item");
        }

        var cellO = fn.findCellBySSID(target.data("ssid"));
        var cell = cellO.cell;
        var modal = $("#networks").find(".modal");
        var form = modal.find("form");
        var passwordInput = form.find("input[name=\"password\"]");
        var passwordLabel = form.find("label[for=\"" + passwordInput.attr("id") + "\"]");
        var cellInput = form.find("input[name=\"cell\"]");
        var modalDialog = modal.find(".modal-dialog");

        modal.find("legend").text(cell["ssid"]);
        passwordInput.val("");
        cellInput.val(cellO.index);

        if(cell["encryption"] == "on") {
            passwordLabel.show();
            passwordInput.show();
        } else {
            passwordLabel.hide();
            passwordInput.hide();
        }

        modal.modal("show");

        modalDialog.css("top", ($(window).height() / 2) - (modalDialog.height() / 2));
        passwordInput.focus();
    };

    app.networkConnectHandler = function(e) {
        e.preventDefault();

        var target = $(e.target);

        if(!target.is("form")) {
            target = target.parents("form");
        }

        var formData = {};

        $.each(target.serializeArray(), function(index, value) {
            formData[value["name"]] = value["value"];
        });

        formData["cell"] = app.vars.cells[parseInt(formData["cell"])];

        $.ajax({
            url: '../api/wlan/connect',
            method: "POST",
            data: formData,
            dataType: 'json',
            success: app.connectionSuccessHandler,
            error: app.errorHandler
        });
    };

    app.connectionSuccessHandler = function(data) {
        console.log(data);
        window.location.reload();
    };

    app.isLoading = function(isLoading) {
        var refreshButton = $("button[name=\"refresh\"]");
        var panelBody = $("#networks-panel").find(".panel-body");

        if(isLoading) {
            refreshButton.addClass("spin");
            panelBody.addClass("loading");
        } else {
            refreshButton.removeClass("spin");
            panelBody.removeClass("loading");
        }
    };

    fn.mergeCellDuplicates = function() {
        var cells = [];

        for(var i = 0; i < app.vars.cells.length; i++) {
            var add = true;
            var cellA = app.vars.cells[i];

            for(var j = 0; j < cells.length; j++) {
                var cellB = cells[j];

                if(cellA["ssid"] != cellB["ssid"]) {
                    continue;
                }

                add = false;

                if(cellA["address"] == cellB["address"]
                    || parseFloat(cellA["quality"]) > parseFloat(cellB["quality"])
                ) {
                    cells[j] = cellA;
                }
            }

            if(add) {
                cells.push(cellA);
            }
        }

        app.vars.cells = cells;
    };

    fn.sortCellsByQualityAndSignal = function(cellA, cellB) {
        var result = parseFloat(cellB["quality"]) - parseFloat(cellA["quality"]);

        if(result == 0) {
            result = parseFloat(cellB["signal_strength"]) - parseFloat(cellA["signal_strength"]);
        }

        if(result == 0) {
            result = cellB["ssid"] < cellA["ssid"];
        }

        return result;
    };

    fn.addLabelValueRowToTable = function($table, label, value) {
        return $table.append(
            $("<tr>").append(
                $("<td>").text(label)
            ).append(
                $("<td>").text(value)
            )
        );
    };

    fn.findCellBySSID = function(ssid) {
        for(var i = 0; i < app.vars.cells.length; i++) {
            if(app.vars.cells[i]["ssid"] == ssid) {
                return {
                    index: i,
                    cell: app.vars.cells[i]
                };
            }
        }
    };

    $(app.init);
})();