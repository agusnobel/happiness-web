(function() {
    window.app = {};
    window.app.vars = {};
    window.fn = {};

    app.init = function() {
        $.material.init();
        app.initSliders();
        app.initCheckboxes();
    };

    app.initSliders = function() {
        var speedSlider = $("#speedSlider");

        speedSlider.noUiSlider({
            start: 100,
            connect: "lower",
            range: {
                min: 0,
                max: 200
            }
        });

        speedSlider.on("change", app.onSpeedChangeHandler);
        speedSlider.on("slide", app.onSpeedUpdateHandler);
        speedSlider.trigger("slide");
    };

    app.onSpeedChangeHandler = function(e) {
        var target = $(e.target);
        var formData = {};
        formData["speed"] = Math.ceil(target.val());

        $.ajax({
            url: '../api/remote/speed',
            method: "POST",
            data: formData,
            dataType: 'json',
            success: app.successHandler,
            error: app.errorHandler
        });
    };

    app.onSpeedUpdateHandler = function(e) {
        var target = $(e.target);
        $("#speedIndicator").text(Math.ceil(target.val()));
    };

    app.initCheckboxes = function() {
        $("input[name=\"rhythm\"]").on("change", app.onRhythmChangeHandler);
        $("input[name=\"vibrating\"]").on("change", app.onVibrationChangeHandler);
    };

    app.onRhythmChangeHandler = function(e) {
        var target = $(e.target);
        var formData = {};

        formData["rhythm"] = target.is(":checked") ? target.val() : 0;
        $("input[name=\"rhythm\"]").not(target).prop("checked", false);

        $.ajax({
            url: '../api/remote/rhythm',
            method: "POST",
            data: formData,
            dataType: 'json',
            success: app.successHandler,
            error: app.errorHandler
        });
    };

    app.onVibrationChangeHandler = function(e) {
        var target = $(e.target);
        var formData = {};
        var vibrationState = $("#vibrationState");

        formData["vibrating"] = target.is(":checked") ? 1 : 0;
        vibrationState.text(vibrationState.data(target.is(":checked") ? "on" : "off"));

        $.ajax({
            url: '../api/remote/vibrating',
            method: "POST",
            data: formData,
            dataType: 'json',
            success: app.successHandler,
            error: app.errorHandler
        });
    };

    app.successHandler = function(data) {
        console.log("Success", data);
    };

    app.errorHandler = function() {
        console.log('Error on scanning', arguments);
    };

    $(app.init);
})();